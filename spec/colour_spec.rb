require_relative '../src/colour'
require_relative 'spec_helper'

describe 'Colour' do
  describe '#new' do
    it 'should raise an error for invalid colour strings' do
      expect {Colour.new('DDWW2W')}.to raise_error ArgumentError
    end 
    
    it 'should accept valid hexadecimal values' do
      Colour.new('39DF0A')
    end 
  end

  let(:colour) {Colour.new('FFEEDD')}
 
  describe '#red' do
    it 'should return the red component of the colour accurately' do      
      expect(colour.red).to eq(255)
    end
  end
  
  describe '#green' do
    it 'should return the green component of the colour accurately' do      
      expect(colour.green).to eq(238)
    end
  end
  
  describe '#red' do
    it 'should return the blue component of the colour accurately' do      
      expect(colour.blue).to eq(221)
    end
  end
  
  describe '#get_colour_string' do
    it 'should return the correct string for supplied values' do
      expect(Colour.get_colour_string(255,255,255)).to eq('FFFFFF')
    end  
    it 'should handle 0 values correctly' do
      expect(Colour.get_colour_string(222,0,174)).to eq('DE00AE')
    end    
  end
end
