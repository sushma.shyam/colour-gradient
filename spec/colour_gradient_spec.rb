# The challenge:
# Find the colour gradient betweem a start colour and an end colour
# The gradient should contain n steps
# 

require_relative 'spec_helper'
require_relative '../src/colour_gradient'

describe 'ColourGradient' do
 
  it "should create a colour gradient object" do
    ColourGradient.new('FEFEFE', '00DD00',5)
  end

  it "should return the correct gradients" do
    cg = ColourGradient.new('000005', '050500',6)
    expect(cg.gradients).to eq(['000005','010104', '020203','030302','040401','050500'])
  end
  
  it "should handle inequal step cases" do
    cg = ColourGradient.new('FEFEFE', '00DD00',5)
    expect(cg.gradients).to eq(['FEFEFE','BEF5BE', '7FED7F','3FE53F','00DD00'])
  end
  
  it "should three steps even when number of steps is below 3" do
    cg = ColourGradient.new('FEFEFE', '00DD00',2)
    expect(cg.gradients).to eq(['FEFEFE', '7FED7F','00DD00'])
  end
  
  it "should be able to handle greater number of steps" do
    cg = ColourGradient.new('00ffdd', '1e4213',64)
    expect(cg.gradients).to eq(["00FFDD", "00FCD9", "00F9D6", "01F6D3", "01F3D0", 
                                "02F0CC", "02EDC9", "03EAC6", "03E7C3", "04E4C0", 
                                "04E1BC", "05DEB9", "05DBB6", "06D8B3", "06D5B0", 
                                "07D2AC", "07CFA9", "08CCA6", "08C9A3", "09C6A0", 
                                "09C39C", "0AC099", "0ABD96", "0ABA93", "0BB790", 
                                "0BB48C", "0CB189", "0CAE86", "0DAB83", "0DA880", 
                                "0EA57C", "0EA279", "0F9F76", "0F9C73", "10996F", 
                                "10966C", "119369", "119066", "128D63", "128A5F", 
                                "13875C", "138459", "148156", "147E53", "147B4F", 
                                "15784C", "157549", "167246", "166F43", "176C3F", 
                                "17693C", "186639", "186336", "196033", "195D2F", 
                                "1A5A2C", "1A5729", "1B5426", "1B5123", "1C4E1F", 
                                "1C4B1C", "1D4819", "1D4516", "1E4213"])
  end
end
