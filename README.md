## Overview

This simple application calculates a linear colour gradient between two colours in number of steps given. 

The colours are represented as RGB Hexadecimal values.

The output contains an array of colours that make up the gradient between the start and the end colours. The colours returned include the start and end colours and the array size is equal to the steps returned. 

If the number of steps mentioned is less than 3, then this application still returns at least 3 colours in the output.

This application behaves like the colour gradient calculator at

http://www.perbang.dk/rgbgradient/

### Installation 

#### Prerequisites

- Ruby 2.x.x
- Bundler gem (can be installed using gem install bundler)

To use this application, one can clone this repository and install the dependacies by running the following command from the command line. 

'bundle install'

This application has been successfully run and tested on Ruby 2.0.0p481 with RSpec 3.0.0

### Instructions for running this application

This application can be run from command line by providing the start colour, end colour and number of steps as arguments to the rake task. 

rake test[start_colour, end_colour, steps]

Example:

rake test['FFFFFF','000000',20]

The start colour and end colour should be valid 6 character hexadecimal strings. The steps argument should be a positive integer

The output consisting of an array of RGB colour strings will be printed on the console
