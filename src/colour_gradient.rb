# contains gradients between a start and a stop colour (including them)
# the number of gradients, start and end colours are configured through the 
# initializer

require_relative 'colour'

class ColourGradient
  
  def initialize(start, stop, steps)
    steps = 3 if steps < 3
    @max_steps = steps
    @start = Colour.new(start)
    @stop = Colour.new(stop)
  end
  
  def gradients
    @gradients ||= compute_gradients 
  end
  
private
  def interpolate(start_value, end_value, step)
    value_difference = end_value - start_value
    increment = step * value_difference/(@max_steps-1)
    
    (start_value + increment).round
  end
  
  def step_red(step)
    interpolate(@start.red, @stop.red, step)
  end
  
  def step_green(step)
    interpolate(@start.green , @stop.green, step)
  end
  
  def step_blue(step)
    interpolate(@start.blue , @stop.blue, step)
  end
  
  def step_colour(step) 
    Colour.get_colour_string(step_red(step), step_green(step), step_blue(step))
  end
  
  def compute_gradients
    (0...@max_steps).collect do |step|
      step_colour(step)
    end
  end  
end
