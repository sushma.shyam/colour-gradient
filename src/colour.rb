# This class represents an RGB colour, 
# It can convert string to r,g,b values and vice-versa
class Colour
  attr_reader :red, :green, :blue
  COLOUR_STRING_FORMAT = /\h{6}/
  COLOUR_STRING_SCAN = /.{2}/

  def initialize(colour_string)
    raise ArgumentError unless colour_string.match(COLOUR_STRING_FORMAT) 
    components = colour_string.scan(COLOUR_STRING_SCAN)
    @red, @green, @blue = components.map(&:hex)
  end
  
  def self.get_colour_string(red,green,blue)
    [red, green, blue].collect { |value|
      value.to_s(16).rjust(2, '0')
    }.join.upcase
  end
end
